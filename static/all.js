(function(a, b) {
    "undefined" != typeof module && module.exports ? module.exports = b() : "function" == typeof define && define.amd ? define([], b) : a.buzz = b();
})(this, function() {
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    var a = {
        audioCtx: window.AudioContext ? new AudioContext() : null,
        defaults: {
            autoplay: !1,
            duration: 5e3,
            formats: [],
            loop: !1,
            placeholder: "--",
            preload: "metadata",
            volume: 80,
            webAudioApi: !1,
            document: window.document
        },
        types: {
            mp3: "audio/mpeg",
            ogg: "audio/ogg",
            wav: "audio/wav",
            aac: "audio/aac",
            m4a: "audio/x-m4a"
        },
        sounds: [],
        el: document.createElement("audio"),
        sound: function(b, c) {
            function d(a) {
                for (var b = [], c = a.length - 1, d = 0; c >= d; d++) b.push({
                    start: a.start(d),
                    end: a.end(d)
                });
                return b;
            }

            function e(a) {
                return a.split(".").pop();
            }

            function f(b, c) {
                var d = g.createElement("source");
                d.src = c, a.types[e(c)] && (d.type = a.types[e(c)]), b.appendChild(d);
            }
            c = c || {};
            var g = c.document || a.defaults.document,
                h = 0,
                i = [],
                j = {},
                k = a.isSupported();
            if (this.load = function() {
                    return k ? (this.sound.load(), this) : this;
                }, this.play = function() {
                    return k ? (this.sound.play(), this) : this;
                }, this.togglePlay = function() {
                    return k ? (this.sound.paused ? this.sound.play() : this.sound.pause(), this) : this;
                }, this.pause = function() {
                    return k ? (this.sound.pause(), this) : this;
                }, this.isPaused = function() {
                    return k ? this.sound.paused : null;
                }, this.stop = function() {
                    return k ? (this.setTime(0), this.sound.pause(), this) : this;
                }, this.isEnded = function() {
                    return k ? this.sound.ended : null;
                }, this.loop = function() {
                    return k ? (this.sound.loop = "loop", this.bind("ended.buzzloop", function() {
                        this.currentTime = 0, this.play();
                    }), this) : this;
                }, this.unloop = function() {
                    return k ? (this.sound.removeAttribute("loop"), this.unbind("ended.buzzloop"), this) : this;
                }, this.mute = function() {
                    return k ? (this.sound.muted = !0, this) : this;
                }, this.unmute = function() {
                    return k ? (this.sound.muted = !1, this) : this;
                }, this.toggleMute = function() {
                    return k ? (this.sound.muted = !this.sound.muted, this) : this;
                }, this.isMuted = function() {
                    return k ? this.sound.muted : null;
                }, this.setVolume = function(a) {
                    return k ? (0 > a && (a = 0), a > 100 && (a = 100), this.volume = a, this.sound.volume = a / 100, this) : this;
                }, this.getVolume = function() {
                    return k ? this.volume : this;
                }, this.increaseVolume = function(a) {
                    return this.setVolume(this.volume + (a || 1));
                }, this.decreaseVolume = function(a) {
                    return this.setVolume(this.volume - (a || 1));
                }, this.setTime = function(a) {
                    if (!k) return this;
                    var b = !0;
                    return this.whenReady(function() {
                        b === !0 && (b = !1, this.sound.currentTime = a);
                    }), this;
                }, this.getTime = function() {
                    if (!k) return null;
                    var b = Math.round(100 * this.sound.currentTime) / 100;
                    return isNaN(b) ? a.defaults.placeholder : b;
                }, this.setPercent = function(b) {
                    return k ? this.setTime(a.fromPercent(b, this.sound.duration)) : this;
                }, this.getPercent = function() {
                    if (!k) return null;
                    var b = Math.round(a.toPercent(this.sound.currentTime, this.sound.duration));
                    return isNaN(b) ? a.defaults.placeholder : b;
                }, this.setSpeed = function(a) {
                    return k ? (this.sound.playbackRate = a, this) : this;
                }, this.getSpeed = function() {
                    return k ? this.sound.playbackRate : null;
                }, this.getDuration = function() {
                    if (!k) return null;
                    var b = Math.round(100 * this.sound.duration) / 100;
                    return isNaN(b) ? a.defaults.placeholder : b;
                }, this.getPlayed = function() {
                    return k ? d(this.sound.played) : null;
                }, this.getBuffered = function() {
                    return k ? d(this.sound.buffered) : null;
                }, this.getSeekable = function() {
                    return k ? d(this.sound.seekable) : null;
                }, this.getErrorCode = function() {
                    return k && this.sound.error ? this.sound.error.code : 0;
                }, this.getErrorMessage = function() {
                    if (!k) return null;
                    switch (this.getErrorCode()) {
                        case 1:
                            return "MEDIA_ERR_ABORTED";
                        case 2:
                            return "MEDIA_ERR_NETWORK";
                        case 3:
                            return "MEDIA_ERR_DECODE";
                        case 4:
                            return "MEDIA_ERR_SRC_NOT_SUPPORTED";
                        default:
                            return null;
                    }
                }, this.getStateCode = function() {
                    return k ? this.sound.readyState : null;
                }, this.getStateMessage = function() {
                    if (!k) return null;
                    switch (this.getStateCode()) {
                        case 0:
                            return "HAVE_NOTHING";
                        case 1:
                            return "HAVE_METADATA";
                        case 2:
                            return "HAVE_CURRENT_DATA";
                        case 3:
                            return "HAVE_FUTURE_DATA";
                        case 4:
                            return "HAVE_ENOUGH_DATA";
                        default:
                            return null;
                    }
                }, this.getNetworkStateCode = function() {
                    return k ? this.sound.networkState : null;
                }, this.getNetworkStateMessage = function() {
                    if (!k) return null;
                    switch (this.getNetworkStateCode()) {
                        case 0:
                            return "NETWORK_EMPTY";
                        case 1:
                            return "NETWORK_IDLE";
                        case 2:
                            return "NETWORK_LOADING";
                        case 3:
                            return "NETWORK_NO_SOURCE";
                        default:
                            return null;
                    }
                }, this.set = function(a, b) {
                    return k ? (this.sound[a] = b, this) : this;
                }, this.get = function(a) {
                    return k ? a ? this.sound[a] : this.sound : null;
                }, this.bind = function(a, b) {
                    if (!k) return this;
                    a = a.split(" ");
                    for (var c = this, d = function(a) {
                            b.call(c, a);
                        }, e = 0; a.length > e; e++) {
                        var f = a[e],
                            g = f;
                        f = g.split(".")[0], i.push({
                            idx: g,
                            func: d
                        }), this.sound.addEventListener(f, d, !0);
                    }
                    return this;
                }, this.unbind = function(a) {
                    if (!k) return this;
                    a = a.split(" ");
                    for (var b = 0; a.length > b; b++)
                        for (var c = a[b], d = c.split(".")[0], e = 0; i.length > e; e++) {
                            var f = i[e].idx.split(".");
                            (i[e].idx == c || f[1] && f[1] == c.replace(".", "")) && (this.sound.removeEventListener(d, i[e].func, !0), i.splice(e, 1));
                        }
                    return this;
                }, this.bindOnce = function(a, b) {
                    if (!k) return this;
                    var c = this;
                    return j[h++] = !1, this.bind(a + "." + h, function() {
                        j[h] || (j[h] = !0, b.call(c)), c.unbind(a + "." + h);
                    }), this;
                }, this.trigger = function(a) {
                    if (!k) return this;
                    a = a.split(" ");
                    for (var b = 0; a.length > b; b++)
                        for (var c = a[b], d = 0; i.length > d; d++) {
                            var e = i[d].idx.split(".");
                            if (i[d].idx == c || e[0] && e[0] == c.replace(".", "")) {
                                var f = g.createEvent("HTMLEvents");
                                f.initEvent(e[0], !1, !0), this.sound.dispatchEvent(f);
                            }
                        }
                    return this;
                }, this.fadeTo = function(b, c, d) {
                    function e() {
                        setTimeout(function() {
                            b > f && b > h.volume ? (h.setVolume(h.volume += 1), e()) : f > b && h.volume > b ? (h.setVolume(h.volume -= 1), e()) : d instanceof Function && d.apply(h);
                        }, g);
                    }
                    if (!k) return this;
                    c instanceof Function ? (d = c, c = a.defaults.duration) : c = c || a.defaults.duration;
                    var f = this.volume,
                        g = c / Math.abs(f - b),
                        h = this;
                    return this.play(), this.whenReady(function() {
                        e();
                    }), this;
                }, this.fadeIn = function(a, b) {
                    return k ? this.setVolume(0).fadeTo(100, a, b) : this;
                }, this.fadeOut = function(a, b) {
                    return k ? this.fadeTo(0, a, b) : this;
                }, this.fadeWith = function(a, b) {
                    return k ? (this.fadeOut(b, function() {
                        this.stop();
                    }), a.play().fadeIn(b), this) : this;
                }, this.whenReady = function(a) {
                    if (!k) return null;
                    var b = this;
                    0 === this.sound.readyState ? this.bind("canplay.buzzwhenready", function() {
                        a.call(b);
                    }) : a.call(b);
                }, k && b) {
                for (var l in a.defaults) a.defaults.hasOwnProperty(l) && void 0 === c[l] && (c[l] = a.defaults[l]);
                if (this.sound = g.createElement("audio"), c.webAudioApi && a.audioCtx && (this.source = a.audioCtx.createMediaElementSource(this.sound), this.source.connect(a.audioCtx.destination)), b instanceof Array)
                    for (var m in b) b.hasOwnProperty(m) && f(this.sound, b[m]);
                else if (c.formats.length)
                    for (var n in c.formats) c.formats.hasOwnProperty(n) && f(this.sound, b + "." + c.formats[n]);
                else f(this.sound, b);
                c.loop && this.loop(), c.autoplay && (this.sound.autoplay = "autoplay"), this.sound.preload = c.preload === !0 ? "auto" : c.preload === !1 ? "none" : c.preload, this.setVolume(c.volume), a.sounds.push(this);
            }
        },
        group: function(a) {
            function b() {
                for (var b = c(null, arguments), d = b.shift(), e = 0; a.length > e; e++) a[e][d].apply(a[e], b);
            }

            function c(a, b) {
                return a instanceof Array ? a : Array.prototype.slice.call(b);
            }
            a = c(a, arguments), this.getSounds = function() {
                return a;
            }, this.add = function(b) {
                b = c(b, arguments);
                for (var d = 0; b.length > d; d++) a.push(b[d]);
            }, this.remove = function(b) {
                b = c(b, arguments);
                for (var d = 0; b.length > d; d++)
                    for (var e = 0; a.length > e; e++)
                        if (a[e] == b[d]) {
                            a.splice(e, 1);
                            break;
                        }
            }, this.load = function() {
                return b("load"), this;
            }, this.play = function() {
                return b("play"), this;
            }, this.togglePlay = function() {
                return b("togglePlay"), this;
            }, this.pause = function(a) {
                return b("pause", a), this;
            }, this.stop = function() {
                return b("stop"), this;
            }, this.mute = function() {
                return b("mute"), this;
            }, this.unmute = function() {
                return b("unmute"), this;
            }, this.toggleMute = function() {
                return b("toggleMute"), this;
            }, this.setVolume = function(a) {
                return b("setVolume", a), this;
            }, this.increaseVolume = function(a) {
                return b("increaseVolume", a), this;
            }, this.decreaseVolume = function(a) {
                return b("decreaseVolume", a), this;
            }, this.loop = function() {
                return b("loop"), this;
            }, this.unloop = function() {
                return b("unloop"), this;
            }, this.setSpeed = function(a) {
                return b("setSpeed", a), this;
            }, this.setTime = function(a) {
                return b("setTime", a), this;
            }, this.set = function(a, c) {
                return b("set", a, c), this;
            }, this.bind = function(a, c) {
                return b("bind", a, c), this;
            }, this.unbind = function(a) {
                return b("unbind", a), this;
            }, this.bindOnce = function(a, c) {
                return b("bindOnce", a, c), this;
            }, this.trigger = function(a) {
                return b("trigger", a), this;
            }, this.fade = function(a, c, d, e) {
                return b("fade", a, c, d, e), this;
            }, this.fadeIn = function(a, c) {
                return b("fadeIn", a, c), this;
            }, this.fadeOut = function(a, c) {
                return b("fadeOut", a, c), this;
            };
        },
        all: function() {
            return new a.group(a.sounds);
        },
        isSupported: function() {
            return !!a.el.canPlayType;
        },
        isOGGSupported: function() {
            return !!a.el.canPlayType && a.el.canPlayType('audio/ogg; codecs="vorbis"');
        },
        isWAVSupported: function() {
            return !!a.el.canPlayType && a.el.canPlayType('audio/wav; codecs="1"');
        },
        isMP3Supported: function() {
            return !!a.el.canPlayType && a.el.canPlayType("audio/mpeg;");
        },
        isAACSupported: function() {
            return !!a.el.canPlayType && (a.el.canPlayType("audio/x-m4a;") || a.el.canPlayType("audio/aac;"));
        },
        toTimer: function(a, b) {
            var c, d, e;
            return c = Math.floor(a / 3600), c = isNaN(c) ? "--" : c >= 10 ? c : "0" + c, d = b ? Math.floor(a / 60 % 60) : Math.floor(a / 60), d = isNaN(d) ? "--" : d >= 10 ? d : "0" + d, e = Math.floor(a % 60), e = isNaN(e) ? "--" : e >= 10 ? e : "0" + e, b ? c + ":" + d + ":" + e : d + ":" + e;
        },
        fromTimer: function(a) {
            var b = ("" + a).split(":");
            return b && 3 == b.length && (a = 3600 * parseInt(b[0], 10) + 60 * parseInt(b[1], 10) + parseInt(b[2], 10)), b && 2 == b.length && (a = 60 * parseInt(b[0], 10) + parseInt(b[1], 10)), a;
        },
        toPercent: function(a, b, c) {
            var d = Math.pow(10, c || 0);
            return Math.round(100 * a / b * d) / d;
        },
        fromPercent: function(a, b, c) {
            var d = Math.pow(10, c || 0);
            return Math.round(b / 100 * a * d) / d;
        }
    };
    return a;
});;
(function(a) {
    if (typeof define === 'function' && define.amd) define(['jquery'], a);
    else if (typeof exports === 'object') a(require('jquery'));
    else a(jQuery);
}(function(a) {
    var b = /\+/g;

    function c(a) {
        return h.raw ? a : encodeURIComponent(a);
    }

    function d(a) {
        return h.raw ? a : decodeURIComponent(a);
    }

    function e(a) {
        return c(h.json ? JSON.stringify(a) : String(a));
    }

    function f(a) {
        if (a.indexOf('"') === 0) a = a.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        try {
            a = decodeURIComponent(a.replace(b, ' '));
            return h.json ? JSON.parse(a) : a;
        } catch (c) {}
    }

    function g(b, c) {
        var d = h.raw ? b : f(b);
        return a.isFunction(c) ? c(d) : d;
    }
    var h = a.cookie = function(b, f, i) {
        if (arguments.length > 1 && !a.isFunction(f)) {
            i = a.extend({}, h.defaults, i);
            if (typeof i.expires === 'number') {
                var j = i.expires,
                    k = i.expires = new Date();
                k.setTime(+k + j * 864e+5);
            }
            return (document.cookie = [c(b), '=', e(f), i.expires ? '; expires=' + i.expires.toUTCString() : '', i.path ? '; path=' + i.path : '', i.domain ? '; domain=' + i.domain : '', i.secure ? '; secure' : ''].join(''));
        }
        var l = b ? undefined : {};
        var m = document.cookie ? document.cookie.split('; ') : [];
        for (var n = 0, o = m.length; n < o; n++) {
            var p = m[n].split('=');
            var q = d(p.shift());
            var r = p.join('=');
            if (b && b === q) {
                l = g(r, f);
                break;
            }
            if (!b && (r = g(r)) !== undefined) l[q] = r;
        }
        return l;
    };
    h.defaults = {};
    a.removeCookie = function(b, c) {
        if (a.cookie(b) === undefined) return false;
        a.cookie(b, '', a.extend({}, c, {
            expires: -1
        }));
        return !a.cookie(b);
    };
}));;
if (!jQuery.browser) {
    jQuery.browser = {};
    jQuery.browser.mozilla = false;
    jQuery.browser.webkit = false;
    jQuery.browser.opera = false;
    jQuery.browser.safari = false;
    jQuery.browser.chrome = false;
    jQuery.browser.msie = false;
    jQuery.browser.android = false;
    jQuery.browser.blackberry = false;
    jQuery.browser.ios = false;
    jQuery.browser.operaMobile = false;
    jQuery.browser.windowsMobile = false;
    jQuery.browser.mobile = false;
    var a = navigator.userAgent;
    jQuery.browser.ua = a;
    jQuery.browser.name = navigator.appName;
    jQuery.browser.fullVersion = '' + parseFloat(navigator.appVersion);
    jQuery.browser.majorVersion = parseInt(navigator.appVersion, 10);
    var b, c, d;
    if ((c = a.indexOf("Opera")) != -1) {
        jQuery.browser.opera = true;
        jQuery.browser.name = "Opera";
        jQuery.browser.fullVersion = a.substring(c + 6);
        if ((c = a.indexOf("Version")) != -1) jQuery.browser.fullVersion = a.substring(c + 8);
    } else if ((c = a.indexOf("MSIE")) != -1) {
        jQuery.browser.msie = true;
        jQuery.browser.name = "Microsoft Internet Explorer";
        jQuery.browser.fullVersion = a.substring(c + 5);
    } else if (a.indexOf("Trident") != -1) {
        jQuery.browser.msie = true;
        jQuery.browser.name = "Microsoft Internet Explorer";
        var e = a.indexOf("rv:") + 3;
        var f = e + 4;
        jQuery.browser.fullVersion = a.substring(e, f);
    } else if ((c = a.indexOf("Chrome")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.chrome = true;
        jQuery.browser.name = "Chrome";
        jQuery.browser.fullVersion = a.substring(c + 7);
    } else if ((c = a.indexOf("Safari")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.safari = true;
        jQuery.browser.name = "Safari";
        jQuery.browser.fullVersion = a.substring(c + 7);
        if ((c = a.indexOf("Version")) != -1) jQuery.browser.fullVersion = a.substring(c + 8);
    } else if ((c = a.indexOf("AppleWebkit")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.name = "Safari";
        jQuery.browser.fullVersion = a.substring(c + 7);
        if ((c = a.indexOf("Version")) != -1) jQuery.browser.fullVersion = a.substring(c + 8);
    } else if ((c = a.indexOf("Firefox")) != -1) {
        jQuery.browser.mozilla = true;
        jQuery.browser.name = "Firefox";
        jQuery.browser.fullVersion = a.substring(c + 8);
    } else if ((b = a.lastIndexOf(' ') + 1) < (c = a.lastIndexOf('/'))) {
        jQuery.browser.name = a.substring(b, c);
        jQuery.browser.fullVersion = a.substring(c + 1);
        if (jQuery.browser.name.toLowerCase() == jQuery.browser.name.toUpperCase()) jQuery.browser.name = navigator.appName;
    }
    jQuery.browser.android = (/Android/i).test(a);
    jQuery.browser.blackberry = (/BlackBerry/i).test(a);
    jQuery.browser.ios = (/iPhone|iPad|iPod/i).test(a);
    jQuery.browser.operaMobile = (/Opera Mini/i).test(a);
    jQuery.browser.windowsMobile = (/IEMobile/i).test(a);
    jQuery.browser.mobile = jQuery.browser.android || jQuery.browser.blackberry || jQuery.browser.ios || jQuery.browser.windowsMobile || jQuery.browser.operaMobile;
    if ((d = jQuery.browser.fullVersion.indexOf(";")) != -1) jQuery.browser.fullVersion = jQuery.browser.fullVersion.substring(0, d);
    if ((d = jQuery.browser.fullVersion.indexOf(" ")) != -1) jQuery.browser.fullVersion = jQuery.browser.fullVersion.substring(0, d);
    jQuery.browser.majorVersion = parseInt('' + jQuery.browser.fullVersion, 10);
    if (isNaN(jQuery.browser.majorVersion)) {
        jQuery.browser.fullVersion = '' + parseFloat(navigator.appVersion);
        jQuery.browser.majorVersion = parseInt(navigator.appVersion, 10);
    }
    jQuery.browser.version = jQuery.browser.majorVersion;
};
!(function(a, b, c, d) {
    "use strict";
    var e = "vide",
        f = {
            volume: 1,
            playbackRate: 1,
            muted: true,
            loop: true,
            autoplay: true,
            position: "50% 50%",
            posterType: "detect",
            resizing: true
        },
        g = /iPad|iPhone|iPod/i.test(d.userAgent),
        h = /Android/i.test(d.userAgent);

    function i(a) {
        var b = {},
            c, d, e, f, g, h, i;
        g = a.replace(/\s*:\s*/g, ":").replace(/\s*,\s*/g, ",").split(",");
        for (i = 0, h = g.length; i < h; i++) {
            d = g[i];
            if (d.search(/^(http|https|ftp):\/\//) !== -1 || d.search(":") === -1) break;
            c = d.indexOf(":");
            e = d.substring(0, c);
            f = d.substring(c + 1);
            if (!f) f = undefined;
            if (typeof f === "string") f = f === "true" || (f === "false" ? false : f);
            if (typeof f === "string") f = !isNaN(f) ? +f : f;
            b[e] = f;
        }
        if (e == null && f == null) return a;
        return b;
    }

    function j(a) {
        a = "" + a;
        var b = a.split(/\s+/),
            c = "50%",
            d = "50%",
            e, f, g;
        for (g = 0, e = b.length; g < e; g++) {
            f = b[g];
            if (f === "left") c = "0%";
            else if (f === "right") c = "100%";
            else if (f === "top") d = "0%";
            else if (f === "bottom") d = "100%";
            else if (f === "center")
                if (g === 0) c = "50%";
                else d = "50%";
            else if (g === 0) c = f;
            else d = f;
        }
        return {
            x: c,
            y: d
        };
    }

    function k(b, c) {
        var d = function() {
            c(this.src);
        };
        a("<img src='http://" + p.getMediaServer() + "/" + b + ".jpg'>").load(d);
    }

    function l(b, c, d) {
        this.$element = a(b);
        if (typeof c === "string") c = i(c);
        if (!d) d = {};
        else if (typeof d === "string") d = i(d);
        if (typeof c === "string") c = c.replace(/\.\w*$/, "");
        else if (typeof c === "object")
            for (var e in c)
                if (c.hasOwnProperty(e)) c[e] = c[e].replace(/\.\w*$/, "");
        this.settings = a.extend({}, f, d);
        this.path = c;
        this.init();
    }
    l.prototype.init = function() {
        var b = this,
            c = j(b.settings.position),
            d, f;
        b.$wrapper = a("<div class='video-container'>").css({
            "position": "fixed",
            "z-index": -1,
            "top": 0,
            "left": 0,
            "bottom": 0,
            "right": 0,
            "overflow": "hidden",
            "-webkit-background-size": "cover",
            "-moz-background-size": "cover",
            "-o-background-size": "cover",
            "background-size": "cover",
            "background-repeat": "no-repeat",
            "background-position": c.x + " " + c.y
        });
        f = b.path;
        if (typeof b.path === "object")
            if (b.path.poster) f = b.path.poster;
            else if (b.path.mp4) f = b.path.mp4;
        else if (b.path.webm) f = b.path.webm;
        else if (b.path.ogv) f = b.path.ogv;
        if (b.settings.posterType === "detect") k(f, function(a) {
            b.$wrapper.css("background-image", "url(" + a + ")");
        });
        else if (b.settings.posterType !== "none") b.$wrapper.css("background-image", "url(http://" + p.getMediaServer() + "/" + f + "." + b.settings.posterType + ")");
        if (b.$element.css("position") === "static") b.$element.css("position", "relative");
        b.$element.prepend(b.$wrapper);
        if (!g && !h) {
            d = "";
            if (typeof b.path === "object") {
                if (b.path.mp4) d += "<source src='http://" + p.getMediaServer() + "/" + b.path.mp4 + ".mp4' type='video/mp4'>";
                if (b.path.webm) d += "<source src='http://" + p.getMediaServer() + "/" + b.path.webm + ".webm' type='video/webm'>";
                if (b.path.ogv) d += "<source src='http://" + p.getMediaServer() + "/" + b.path.ogv + ".ogv' type='video/ogv'>";
                b.$video = a("<video>" + d + "</video>");
            } else b.$video = a("<video>" + "<source src='http://" + p.getMediaServer() + "/" + b.path + ".mp4' type='video/mp4'>" + "<source src='http://" + p.getMediaServer() + "/" + b.path + ".webm' type='video/webm'>" + "<source src='http://" + p.getMediaServer() + "/" + b.path + ".ogv' type='video/ogg'>" + "</video>");
            b.$video.css("visibility", "hidden");
            b.$video.prop({
                autoplay: b.settings.autoplay,
                loop: b.settings.loop,
                volume: b.settings.volume,
                muted: b.settings.muted,
                playbackRate: b.settings.playbackRate
            });
            b.$wrapper.append(b.$video);
            b.$video.css({
                "margin": "auto",
                "position": "fixed",
                "z-index": -1,
                "top": c.y,
                "left": c.x,
                "-webkit-transform": "translate(-" + c.x + ", -" + c.y + ")",
                "-ms-transform": "translate(-" + c.x + ", -" + c.y + ")",
                "transform": "translate(-" + c.x + ", -" + c.y + ")"
            });
            b.$video.bind("loadedmetadata." + e, function() {
                b.$video.css("visibility", "visible");
                b.resize();
                b.$wrapper.css("background-image", "none");
            });
            b.$video.bind("canplaythrough." + e, function() {
                setTimeout(function() {
                    a(".video-fader").removeClass('faded-full');
                }, 100);
            });
            b.$element.bind("resize." + e, function() {
                if (b.settings.resizing) b.resize();
            });
        } else a(".video-fader").removeClass('faded-full');
    };
    l.prototype.getVideoObject = function() {
        return this.$video ? this.$video[0] : null;
    };
    l.prototype.resize = function() {
        if (!this.$video) return;
        var a = this.$video[0].videoHeight,
            b = this.$video[0].videoWidth,
            c = this.$wrapper.height(),
            d = this.$wrapper.width();
        if (d / b > c / a) this.$video.css({
            "width": d + 2,
            "height": "auto"
        });
        else this.$video.css({
            "width": "auto",
            "height": c + 2
        });
    };
    l.prototype.destroy = function() {
        this.$element.unbind(e);
        if (this.$video) this.$video.unbind(e);
        delete a[e].lookup[this.index];
        this.$element.removeData(e);
        this.$wrapper.remove();
    };
    a[e] = {
        lookup: []
    };
    a.fn[e] = function(b, c) {
        var d;
        this.each(function() {
            d = a.data(this, e);
            if (d) d.destroy();
            d = new l(this, b, c);
            d.index = a[e].lookup.push(d) - 1;
            a.data(this, e, d);
        });
        return this;
    };
    a(c).ready(function() {
        a(b).bind("resize." + e, function() {
            for (var b = a[e].lookup.length, c = 0, d; c < b; c++) {
                d = a[e].lookup[c];
                if (d && d.settings.resizing) d.resize();
            }
        });
        a(c).find("[data-" + e + "-bg]").each(function(b, c) {
            var d = a(c),
                f = d.data(e + "-options"),
                g = d.data(e + "-bg");
            d[e](g, f);
        });
    });
})(window.jQuery, window, document, navigator);;
$(document).ready(function() {
    var a = 1000;
    var b = 30000;
    var c = 90;
    var d = 3000;
    var e = 100;
    if ($('.top-center-logo-logo').length) {
        $('.center-logo').addClass('shown');
        var f = function(a, b) {
            var f = arguments.callee;
            var g = $('.top-center-logo-logo .note-' + a).empty();
            var h = g.data('text') || '';
            var i = 1;
            g.fadeIn(1400, function() {
                setTimeout(function() {
                    if (h) {
                        g.text(h.substring(0, i));
                        i++;
                    }
                    if (h && i <= h.length) setTimeout(arguments.callee, c);
                    else setTimeout(function() {
                        g.fadeOut(1400, function() {
                            if (a < 5) setTimeout(function() {
                                f(a + 1, b);
                            }, e);
                            else b();
                        });
                    }, d);
                }, c);
            });
        };
        setTimeout(function() {
            var a = arguments.callee;
            f(1, function() {
                setTimeout(a, b);
            });
        }, a);
    }
});;
var g = {
    quotes: ['USDKZT', 'EURKZT', 'RUBKZT'],
    elements: {},
    $clientsOnline: $('.clients-online span'),
    $body: $('body'),
    $rubbrent: $('.rubbrent'),
    $eurusd: $('.eurusd'),
    showed: false,
    lastValuesUpdate: [0, 0, 0, 0],
    valuesUpdateInterval: 2000,
    usersUpdateInterval: 4000,
    initElements: function() {
        for (var a in this.quotes)
            if (this.quotes.hasOwnProperty(a)) {
                this.elements[a] = {};
                this.elements[a].$container = $('.quotes .item.' + this.quotes[a]);
                this.elements[a].$cell = $('.quotes .item.' + this.quotes[a] + ' .value');
                this.elements[a].$note = $('.quotes .item.' + this.quotes[a] + ' .note span');
            }
    },
    updateValues: function(a) {

        for(var i = 0; i < a.length; i++) {
            item = a[i]; 
            this.lastValuesUpdate[0] = new Date().getTime();
            var last_rate =  parseFloat($('.quotes .item.' + item.symbol + ' div.value').text());
            if (last_rate == item.rate)
                continue;
            $('.quotes .item.' + item.symbol).removeClass('plus').removeClass('minus');
            $('.quotes .item.' + item.symbol + ' div.value').html(p.formatFloatVal(item.rate));
            if (item.inc == 1){
                $('.quotes .item.' + item.symbol).addClass('plus');
            }
            else {
                $('.quotes .item.' + item.symbol).addClass('minus');
            }
            $('.quotes-mini .item .note.' + item.symbol + ' span').text(p.formatFloatVal(item.rate));

            if (item.symbol == 'GASOLINE-92' || item.symbol == 'GASOLINE-95'){
                $('.quotes-mini .item.' + item.symbol + ' span.value').text(p.formatIntVal(item.rate));
            }else{
                $('.quotes-mini .item.' + item.symbol + ' span.value').text(p.formatFloatVal(item.rate));
            }
            if (item.inc == 1){
                $('.quotes-mini .item.' + item.symbol).addClass('plus');
            }
            else {
                $('.quotes-mini .item.' + item.symbol).addClass('minus');
            }
            $('.quotes-day .item.' + item.symbol + ' .note').text(item.text);
        }

        /*for (var b = 0; b < 3; b++)
            if (new Date().getTime() - this.lastValuesUpdate[b] > this.valuesUpdateInterval) {
                this.lastValuesUpdate[b] = new Date().getTime();
                var c = p.getSignClass(a[b], this.elements[b].$cell.text(), b != 2);
                if (c) this.elements[b].$container.removeClass('plus').removeClass('minus');
                this.elements[b].$container.addClass(c);
                this.elements[b].$cell.text(p.formatFloatVal(a[b]));
            }*/
    },
    initWatchDog: function() {
        var a = this;
        setInterval(function() {
            if (new Date().getTime() - a.lastValuesUpdate[0] > 120){
                a.initWebSockets();
            }
        }, 3600000);
        setTimeout(function() {
            document.location.reload();

        }, 3600000);
    },
    initWebSockets: function() {
        var a = this;
        setInterval(function(){
        var dd = new Date().getTime();
        $.get( "http://kzt.today/info.json?" + dd, function( data ) {
            a.updateValues(data);
        });
    }, 10001);
        this.initWatchDog();
    },
    updateView: function() {
        /*$('#container .quotes').toggle(!o.getOther('hideData'));
        $('#container .note.comment').toggle(!o.getOther('hideNotes'));
        $('#container .note.additional').toggle(!o.getOther('hideAdditionalDigits'));
        $('.datetime').toggle(!o.getOther('hideClock'));*/
    },
    init: function(a) {
        this.initElements();
        this.updateValues(a);
        this.initWebSockets();
    }
};
$(document).ready(function() {
    current = $.parseJSON(current);
    g.init(current);
    q.init();
    l.init();
    j();
    i();
    k();
    if ($.browser.opera && $.browser.version <= 12 || $.browser.msie) {
        function a() {
            if ($(window).width() > $(window).height()) {
                $('.quotes .item').css('width', (Math.floor(99 / $('.quotes .item').length)) + "%").css('margin-bottom', '0');
                $('.quotes .item .value').css('font-size', $('.quotes .item').width() / 3.1);
                setTimeout(function() {
                    $('.quotes').css('margin-top', -$('.quotes').height() / 2);
                }, 0);
            } else {
                $('.quotes .item').css('width', 100 + "%").css('margin-bottom', '5%');
                $('.quotes .item .value').css('font-size', $('.quotes .item').width() / 5);
                setTimeout(function() {
                    $('.quotes').css('margin-top', 0);
                }, 0);
            }
        }
        a();
        $(window).on('resize', a);
    }
});

function i() {
    setInterval(function() {
        $('.grid-button').toggleClass('hl');
    }, 4000);
}

function j() {
    var a = $('.datetime .time');
    setInterval(function() {
        var b = new Date();
        var c = b.getMinutes();
        a.text(b.getHours() + ':' + (c < 10 ? "0" : "") + c);
    }, 1000);
}

function k() {
    o.load();
    g.updateView();
    $('#disable-digits').prop('checked', o.getOther('hideData')).on('click', function() {
        o.setOther('hideData', $(this).is(':checked'));
        $('#disable-comments, #disable-smalldigits').prop('disabled', $(this).is(':checked'));
        g.updateView();
    });
    $('#disable-comments').prop('checked', o.getOther('hideNotes')).prop('disabled', o.getOther('hideData')).on('click', function() {
        o.setOther('hideNotes', $(this).is(':checked'));
        g.updateView();
    });
    $('#disable-smalldigits').prop('checked', o.getOther('hideAdditionalDigits')).prop('disabled', o.getOther('hideData')).on('click', function() {
        o.setOther('hideAdditionalDigits', $(this).is(':checked'));
        g.updateView();
    });
    $('#disable-clock').prop('checked', o.getOther('hideClock')).on('click', function() {
        o.setOther('hideClock', $(this).is(':checked'));
        g.updateView();
    });
};
var l = {
    musicTracks: [],
    currentMusic: $.cookie('music'),
    $musicCycleCheckbox: $('#music-cycle'),
    $musicEnableCheckbox: $('#music-enable'),
    $container: $('.tab-music .scroll-container'),
    playerObject: new buzz.sound(),
    initSwitcher: function() {
        var a = this;
        var b = $('.audio-switcher');
        for (var c in this.musicTracks) b.append('<a href="' + this.musicTracks[c] + '" ' + (this.currentMusic == this.musicTracks[c] ? 'class="active"' : '') + '" data-name="' + this.musicTracks[c] + '">' + this.musicTracks[c] + '</a>');
        b.on('click', 'a', function(b) {
            a.switchMusic($(this).attr('href'));
            $(this).addClass('active').siblings().removeClass('active');
            return false;
        });
        a.$musicCycleCheckbox.click(function() {
            $.cookie('cycleMusic', $(this).is(':checked') ? 1 : 0, {
                expires: 1000
            });
        }).prop('checked', a.isAutoCycleOn());
        a.$musicEnableCheckbox.click(function() {
            $.cookie('sound', $(this).is(':checked') ? '' : 'off', {
                expires: 1000
            });
            if ($(this).is(':checked')) a.turnOn();
            else a.turnOff();
        }).prop('checked', a.isMusicOn());
        if (!a.isMusicOn()) a.disableMusicUi();
    },
    isMusicOn: function() {
        return $.cookie('sound') != 'off';
    },
    isAutoCycleOn: function() {
        return $.cookie('cycleMusic') == 1;
    },
    turnOffAutoCycle: function() {
        $.cookie('cycleMusic', 0);
        this.$musicCycleCheckbox.prop('checked', false);
    },
    switchMusic: function(a) {
        $.cookie('music', a);
        this.turnOffAutoCycle();
        this.playerObject.fadeOut().stop();
        this.playMusic(a);
    },
    turnOn: function() {
        this.enableMusicUi();
        this.playerObject.play().fadeTo(30).loop();
    },
    turnOff: function() {
        this.disableMusicUi();
        this.playerObject.stop();
    },
    enableMusicUi: function() {
        this.$container.removeClass('disabled');
        this.$musicCycleCheckbox.prop('disabled', false).parent().removeClass('disabled');
    },
    disableMusicUi: function() {
        this.$container.addClass('disabled');
        this.$musicCycleCheckbox.prop('disabled', true).parent().addClass('disabled');
    },
    playMusic: function(a) {
        this.playerObject = new buzz.sound('http://' + p.getMediaServer() + '/sounds/' + a, {
            formats: ["ogg", "mp3", "aac"],
            volume: 0
        });
        this.startAutoCycle();
        if (this.isMusicOn()) this.turnOn();
    },
    startAutoCycle: function() {
        var a = this;
        this.playerObject.bind('seeking', function() {
            if (a.isAutoCycleOn() && a.isMusicOn()) {
                var b = Math.floor(Math.random() * a.musicTracks.length);
                a.switchMusic(a.musicTracks[b]);
                $('.audio-switcher a[data-name="' + a.musicTracks[b] + '"]').addClass('active').siblings().removeClass('active');
            }
        });
    },
    init: function() {
        if (!this.currentMusic || this.musicTracks.indexOf(this.currentMusic) == -1) this.currentMusic = this.musicTracks[0];
        this.initSwitcher();
        //this.playMusic(this.currentMusic);
    }
};
var o = {
    other: {
        hideData: false,
        hideNotes: false,
        hideAdditionalDigits: false,
        hideClock: false
    },
    getBackgroundMode: function() {
        var a = $.cookie('backgroundMode');
        if (a != 'video' && a != 'photo') a = 'video';
        return a;
    },
    setBackgroundMode: function(a) {
        if (a != 'video' && a != 'photo') a = 'video';
        $.cookie('backgroundMode', a, {
            expires: 1000
        });
    },
    setOther: function(a, b) {
        this.other[a] = b;
        this.save();
    },
    getOther: function(a) {
        return this.other[a];
    },
    save: function() {
        $.cookie('settings', JSON.stringify(this.other), {
            expires: 1000
        });
    },
    load: function() {
        var a;
        try {
            a = $.parseJSON($.cookie('settings'));
            this.other = a;
        } catch (b) {}
    }
};
var p = {
    formatFloatVal: function(a) {
        return parseFloat(a).toFixed(2).replace(".", ",");
    },
    formatIntVal: function(a) {
        return parseInt(a);
    },
    getSignClass: function(a, b, c) {
        a = parseFloat(a).toFixed(2);
        b = parseFloat(b.replace(",", ".")).toFixed(2);
        if (a > b) return c ? 'minus' : 'plus';
        if (a < b) return c ? 'plus' : 'minus';
        return false;
    },
    getMainDomainPart: function() {
        return window.location.hostname.split('.').slice(-2).join('.');
    },
    pluralize: function(a, b) {
        a = Math.round(a);
        if (a > 100) a = a % 100;
        firstDigit = a % 10;
        secondDigit = Math.floor(a / 10);
        if (secondDigit != 1)
            if (firstDigit == 1) return b[0];
            else if (firstDigit > 1 && firstDigit < 5) return b[1];
        else return b[2];
        else return b[2];
    },
    getMediaServer: function() {
        return "kzt.today";
    },
    isMobile: function() {
        return (/Android/i.test(navigator.userAgent) || /iPhone|iPad|iPod/i.test(navigator.userAgent));
    }
};
var q = {
    videos: ['alfarabi'],
    currentVideo: $.cookie('video1'),
    $container: $("#container"),
    $videoFader: $('.video-fader'),
    $videoCycleCheckbox: $('#video-cycle'),
    autoCycleTime: 120000,
    initSwitcher: function() {
        var a = this;
        var b = $('.video-switcher');
        for (var c in this.videos) b.append('<a href="' + this.videos[c] + '" ' + (o.getBackgroundMode() == 'video' && this.currentVideo == this.videos[c] ? 'class="active"' : '') + ' style="background-image:url(video/75x75/' + this.videos[c] + '.jpg)" data-name="' + this.videos[c] + '"></a>');
        b.on('click', 'a', function(b) {
            m.off();
            o.setBackgroundMode('video');
            a.turnOffAutoCycle();
            a.switchVideo($(this).attr('href'));
            $(this).addClass('active').siblings().removeClass('active');
            return false;
        });
        a.$videoCycleCheckbox.click(function() {
            $.cookie('cycleVideo', $(this).is(':checked') ? 1 : 0, {
                expires: 1000
            });
            if (o.getBackgroundMode() != 'video') {
                m.off();
                o.setBackgroundMode('video');
                a.startAutoCycle(true);
            }
        }).prop('checked', a.isAutoCycleOn());
    },
    isAutoCycleOn: function() {
        return $.cookie('cycleVideo') == 1;
    },
    turnOffAutoCycle: function() {
        $.cookie('cycleVideo', 0);
        this.$videoCycleCheckbox.prop('checked', false);
    },
    switchVideo: function(a) {
        $(".video-fader").addClass('faded-full');
        var b = this;
        $.cookie('video1', a, {
            expires: 1000
        });
        setTimeout(function() {
            b.playVideo(a);
        }, 300);
    },
    playVideo: function(a) {
        this.$container.vide('video/' + a).removeClass().addClass(a);
    },
    off: function() {
        this.turnOffAutoCycle();
        $('.video-switcher a.active').removeClass('active');
        if (this.$container.data("vide")) this.$container.data("vide").destroy();
    },
    startAutoCycle: function(a) {
        var b = this;
        var c = function() {
            if (b.isAutoCycleOn() && o.getBackgroundMode() == 'video') {
                var a = Math.floor(Math.random() * b.videos.length);
                b.switchVideo(b.videos[a]);
                $('.video-switcher a[data-name="' + b.videos[a] + '"]').addClass('active').siblings().removeClass('active');
            }
        };
        if (a == true) {
            if (b.cycleTimer) clearInterval(b.cycleTimer);
            c();
        }
        b.cycleTimer = setInterval(function() {
            c();
        }, this.autoCycleTime);
    },
    init: function() {
        if (!this.currentVideo || this.videos.indexOf(this.currentVideo) == -1) this.currentVideo = this.videos[0];
        this.initSwitcher();
        this.startAutoCycle();
        if (o.getBackgroundMode() == 'video') this.playVideo(this.currentVideo);
    }
};