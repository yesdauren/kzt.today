<!DOCTYPE html>
<html><head lang="ru"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Тенге: рожденный летать плавать не захочет</title>
<meta charset="UTF-8">
<?php
    $info = file_get_contents('./info.json', true);
?>

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">

<meta name="Description" content="Тенге дальнего плавания: наблюдение за курсами доллара, рубля и прочего.">
<meta name="Keywords" content="курс тенге, курса доллара, валюта, котировки, цены, нацбанк, казахстан">
<meta name="author" content="Даурен Есмуханов">

<meta property="og:type" content="website">
<meta property="og:title" content="Тенге: рожденный летать плавать не захочет">
<meta property="og:description" content="Тенге дальнего плавания: наблюдение за курсами доллара, рубля и прочего.">
<meta property="og:url" content="http://kzt.today">
<meta property="og:site_name" content="kzt.today">
<meta name="twitter:card" content="Тенге дальнего плавания: наблюдение за курсами доллара, рубля и прочего.">
<meta name="twitter:site" content="@kzt.today">
<meta name="twitter:title" content="Тенге: рожденный летать плавать не захочет">
<meta name="twitter:description" content="Тенге дальнего плавания: наблюдение за курсами доллара, рубля и прочего.">
<meta name="twitter:domain" content="kzt.today">

<link href='http://fonts.googleapis.com/css?family=Philosopher:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link href="./static/main.css" rel="stylesheet" type="text/css">

<link rel="icon" href="http://kzt.today/static/favicon.ico" type="image/x-icon">
<style type="text/css"></style>
</head>
<body>
    <span style="display:none"><a href="http://ansar.xyz">Коржын, тойбастар, калта, курак корпе, ак жол заказ по Павлодарской области, Павлодар, Аксу, Экибастуз</a> </span>
    <script type="text/javascript">
    var current = '<?php echo $info ?>';
    </script>
<div id="container" data-vide-options="muted: true, volume: 0.02">
    <div class="video-fader"></div>

    <div class="quotes-day">
        <div class="item DAY">
         <!--   <div class="note comment"></div>-->
        </div>
    </div>
    <div class="quotes">
        <div class="item USDKZT">
            <div class="value"></div>
            <div class="note comment usd"><span>тенге</span> за доллар</div>
            <!--<div class="note additional BRENT">$<span></span> за баррель</div>-->
        </div>
        <div class="item RUBKZT">
            <div class="value"></div>
            <div class="note comment rub"><span>тенге</span> за рубль</div>
        </div>
        <div class="item EURKZT">
            <div class="value"></div>
            <div class="note comment eur"><span>тенге</span> за евро</div>
        </div>
    </div>
    <div class="quotes-mini">
        <div class="item GASOLINE-95">
            <div class="note additional">АИ-95: <span class="value"></span></div>
        </div>
        <div class="item GASOLINE-92">
            <div class="note additional">АИ-92: <span class="value"></span></div>
        </div>
        <div class="item ">
            <div class="note additional BRENT">$<span></span> за баррель</div>
        </div>
    </div>
</div>
<div class="b_footer__branches">
    <a target="_blank" onclick="Share.vkontakte()" class="vk"><img src = "./static/vk.png" alt = "VK"/></a>
    <a target="_blank" onclick="Share.twitter()" class="tw"><img src = "./static/tw.png" alt = "Twitter"/></a>
    <a target="_blank" onclick="Share.facebook()" class="fb"><img src = "./static/fb.png" alt = "Facebook"/></a>
</div>
<div style="display: none;">
<a href="http://kzt.today">Курс тенге к доллару</a>
<a href="http://ansar.xyz">Коржын тойбастар на заказ</a>
<a href="http://iphonelux.kz">Купить iPhone 6S / iPhone 6S Plus / Apple Watch / iPad Pro / iPad Air2 / iPad Mini4 в Алматы</a>
<a href="http://nomobile.kz">Подключай тариф «Вcё» от #beeline</a>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="./static/all.js"></script>
<script type="text/javascript">
    purl = "http://kzt.today";
    ptitle = $('title').text();
    pimg = "http://kzt.today/static/share-image.png";
    text = $('meta[name=Description]').attr('content') + '\n' + $('.quotes-day .item .note').text();
Share = {
    vkontakte: function() {
        url  = 'http://vkontakte.ru/share.php?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image='       + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.popup(url);
    },
    facebook: function() {
       /* url  = 'http://www.facebook.com/sharer.php?s=100';
        url += '&p[title]='     + encodeURIComponent(ptitle);
        url += '&p[summary]='   + encodeURIComponent(text);
        url += '&p[url]='       + encodeURIComponent(purl);
        url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.popup(url);*/
        url  = 'http://www.facebook.com/sharer.php?';
        url += '&description='     + encodeURIComponent(ptitle);
        url += '&u='       + encodeURIComponent(purl);
        url += '&media=' + encodeURIComponent(pimg);
        Share.popup(url);
    },
    twitter: function() {
        url  = 'http://twitter.com/share?';
        url += 'text='      + encodeURIComponent(ptitle);
        url += '&url='      + encodeURIComponent(purl);
        url += '&counturl=' + encodeURIComponent(purl);
        Share.popup(url);
    },

    popup: function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    }
};

</script>

    <!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter32182584 = new Ya.Metrika({ id:32182584, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/32182584" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
</body></html>