# -*- coding: utf-8 -*-
import requests 
import sqlite3
from bs4 import BeautifulSoup
import time

conn = sqlite3.connect('index.db')
c = conn.cursor()


# Create table
#c.execute('''CREATE TABLE stocks
#             (date datetime default current_timestamp, symbol, rate real)''')

#c.execute('''DROP TABLE titles''')

#c.execute('''CREATE TABLE titles
#             (date datetime default current_timestamp, text text, msisdn text, nickname text)''')

#c.execute("""INSERT INTO  titles (text, msisdn, nickname) VALUES
#            ('Цены на бензин в Алматы: От 127 до 130 тенге за литр АИ-92/93', '77015209278', 'DAUREN YESMUKHANOV')""")

#Client ID (Consumer Key)
#dj0yJmk9OWxJUlhjUDVRZVZzJmQ9WVdrOWEwWm1kRmRETkdFbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD03NQ--

#Client Secret (Consumer Secret)
#82b063500bb9d638311b5cd9c08899abd11910d9


#Client ID (Consumer Key)
#dj0yJmk9NVNYaGFJQ2ptYmJIJmQ9WVdrOWVUSmphMHBRTjJVbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yNg--
#Client Secret (Consumer Secret)
#0574e411f8a451dbbe09df17d6540b1a17c2902e

import urllib2, urllib, json

def get_stock_value(symbol):
	c.execute("SELECT rate FROM stocks WHERE symbol='%s' ORDER BY date DESC LIMIT 1" % symbol)
	value = c.fetchone()
	if value is not None:
		return value[0]
	return 0

def update_xchange_2():
	base_url = "https://query.yahooapis.com/v1/public/yql?"
	yql_query = """select * from yahoo.finance.xchange where pair in ("USDKZT","EURKZT", "RUBKZT")"""
	yql_url = base_url + urllib.urlencode({'q':yql_query}) + "&format=json" + "&env=store://datatables.org/alltableswithkeys"
	result = urllib2.urlopen(yql_url).read()
	data = json.loads(result)
	rates = data['query']['results']['rate']
	for rate in rates:
		last_value = get_stock_value(rate['id'])
		current_value = rate['Rate']
		if (last_value!=current_value):
			c.execute("INSERT INTO stocks (symbol, rate) VALUES ('%s',%s)" % (rate['id'], current_value))
			conn.commit()

def update_xchange():
	base_url = "http://tenge.today/category.php"
	result = requests.get(base_url).json()
	data = result
	if data['message'] != 'Success':
		return
	rates = {}
	rates['USDKZT'] = []
	rates['RUBKZT'] = []
	rates['EURKZT'] = []
	categories = data['category']
	for category in categories:
		currencies = category['currency']
		for currency in currencies:
			symbol = currency['code'] + 'KZT'
			buy = float(currency['buy'])
			sell = float(currency['sell'])
			if sell + buy <= 0:
				continue
			if symbol in ["USDKZT","EURKZT", "RUBKZT"]:
				rates[symbol].append((sell + buy)/2)
	for symbol, value in rates.items():
		last_value = get_stock_value(symbol)
		l = rates[symbol]
		current_value = reduce(lambda x, y: x + y, l)/float(len(l))	
		if abs(last_value - current_value) > 0.01:
			c.execute("INSERT INTO stocks (symbol, rate) VALUES ('%s',%s)" % (symbol, current_value))
			conn.commit()

def update_oil():
	symbol = "BRENT"
	base_url = "https://www.theice.com/marketdata/DelayedMarkets.shtml?getContractsAsJson=&productId=254&hubId=403"
	result = urllib2.urlopen(base_url).read()
	data = json.loads(result)
	current_value =  data[0]['lastPrice']
	last_value = get_stock_value(symbol)
	if (last_value!=current_value):
			c.execute("INSERT INTO stocks (symbol, rate) VALUES ('%s',%s)" % (symbol, current_value))
			conn.commit()
	symbol = "WTI"
	base_url = "https://www.theice.com/marketdata/DelayedMarkets.shtml?getContractsAsJson=&productId=425&hubId=619"
	result = urllib2.urlopen(base_url).read()
	data = json.loads(result)
	current_value =  data[0]['lastPrice']
	last_value = get_stock_value(symbol)
	if (last_value!=current_value):
			c.execute("INSERT INTO stocks (symbol, rate) VALUES ('%s',%s)" % (symbol, current_value))
			conn.commit()

def update_gasoline():
	base_url = "http://helios.kz/toplivo/tseny-na-benzin/"
	r = requests.get(base_url)
	html = BeautifulSoup(r.text)
	lis = html.select("#petroil-prices li")
	for li in lis:
		symbol = li.select("span.name")[0].string
		if symbol is not None and len(symbol) == 2:
			symbol = "GASOLINE-" + symbol.upper()
			current_value = int(li.select("span.value")[0].string)
			if current_value is not None and current_value > 0:
				last_value = get_stock_value(symbol)
				if (last_value!=current_value):
					c.execute("INSERT INTO stocks (symbol, rate) VALUES ('%s',%s)" % (symbol, current_value))
					conn.commit()



while (1):
	update_gasoline()
	update_oil()
	update_xchange()
	items = []
	data = {}
	symbol = ""
	rate = 0
	i = 0
	for row in c.execute("""SELECT date, symbol, rate FROM stocks a WHERE a."ROWID" IN (
	    SELECT b."ROWID" 
	    FROM stocks b 
	    WHERE b."ROWID" IS NOT NULL 
	      AND a."symbol" = b."symbol" 
	    ORDER BY b."date" DESC
	    LIMIT 2
	)
	ORDER BY a."symbol", a."date" DESC;"""):
		if (symbol != "" and symbol != row[1]):
			data['inc'] = 1
			items.append(data)
			i = 1
			symbol = row[1]
			rate = row[2]
			data['date'] = row[0]
			data['symbol'] = symbol
			data['rate'] = rate
		elif (symbol == row[1] and i == 1):
			if (rate >= row[2]):
				data['inc'] = 1
			else:
				data['inc'] = 0
			items.append(data)
			symbol = ""
			rate = 0 
			i = 0
			data = {}
		elif (symbol == "" and i == 0):
			i = i + 1
			symbol = row[1]
			rate = row[2]
			data['date'] = row[0]
			data['symbol'] = symbol
			data['rate'] = rate
	if (i == 1):
		items.append(data)
	for row in c.execute("""SELECT text FROM titles ORDER BY date DESC LIMIT 1"""):
		data = {}
		data['symbol'] = 'DAY'
		data['text'] = row[0]
		items.append(data)
	json_data = json.dumps(items)
	f = open('info.json','w')
	f.write(json_data)
	f.close()
	print "done\n"
	conn.close()
	conn = sqlite3.connect('index.db')
	c = conn.cursor()
	time.sleep(60)


